<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <script type="module" crossorigin src="{{ asset('assets/index.0256c957.js') }}"></script>
    <link rel="modulepreload" href="{{ asset('assets/vendor.b1e86581.js') }}">
    <link rel="stylesheet" href="{{ asset('assets/index.2acfb5d8.css') }}">
</head>

<body class="antialiased">
    <div id="app"></div>
</body>

</html>